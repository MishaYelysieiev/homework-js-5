const simpleObject = {
    name: "Simon",
    age: "24",
    sex: "male",
    parent: {
        mother: "Nili",
        father: "Tom"
    }
}

cloneObject = (object) => {
    let objectCopy = {};
    for(let key in object) {
        let prop = object[key];
        objectCopy[key] = typeof prop == "object" ? cloneObject(prop) : prop;
    }    
    return objectCopy;
}

const copiedObject = cloneObject(simpleObject);

copiedObject.name = "Rachel";
copiedObject.sex = "female";
copiedObject.parent.mother = "Helen";
copiedObject.parent.father = "Steve";
console.log(simpleObject);
console.log(copiedObject);